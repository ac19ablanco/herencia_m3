
package herencia5;

import java.util.ArrayList;


public class Lista<E> extends ArrayList<E>{
    
    @Override
    public boolean add(E e) {
        boolean afegit=false;       
        
        if(!contains(e)){
            super.add(e);
            afegit=true;
        }
        return afegit;
    }
    
}
