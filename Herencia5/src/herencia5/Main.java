
package herencia5;

public class Main {

    public static void main(String[] args) {
        

        Lista<Empleat> empleats = new Lista<>();
        
        Empleat empleat1 = new Empleat("Alfonso");
        Directiu empleat2 = new Directiu("Maria");
        Operari empleat3 = new Operari("Juan");
        Oficial empleat4 = new Oficial("Laura");
        Tecnic empleat5 = new Tecnic("Pau");
        
        empleats.add(empleat1);
        empleats.add(empleat2);
        empleats.add(empleat3);
        empleats.add(empleat4);
        empleats.add(empleat5);
        
        System.out.println("Llista: \n"+ empleats);
        
        Lista<Operari> operaris = new Lista<>();
        
        System.out.println("---------------");
        System.out.println("Empleats Operaris:");
        empleats.stream().filter((emp) -> (emp instanceof Operari)).forEachOrdered((emp) -> {
            System.out.println(emp);
        });
        
        
                
    }
    
}
