
package herencia5;

public class Empleat {

    private String nom;

    public String getNombre() {
        return nom;
    }

    public Empleat(String nombre) {
        this.nom = nombre;
    }

    @Override
    public String toString() {
        return "Empleado " + nom;
    }
    
    
}
